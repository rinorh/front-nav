import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ProfileBoxComponent } from './profileBox/profilebox.component';


const routes: Routes = [
  { path: "profile", component: ProfileBoxComponent }
  // { path: "threads", component:  null },
  // { path: "room/:id", component:  null },
  // { path: "directMessage/:id", component:  null },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
