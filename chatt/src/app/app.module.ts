import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProfileComponent } from './navigation/profile/profile.component';
import { NotificationsComponent } from './navigation/notifications/notifications.component';
import { RoomsComponent } from './navigation/rooms/rooms.component';
import { DirectMessagesComponent } from './navigation/directMessages/directMessages.component';
import { RoomComponent } from './navigation/rooms/room/room.component';
import { ThreadComponent } from './navigation/notifications/thread/thread.component';
import { DirectMessageComponent } from './navigation/directMessages/directMessage/directMessage.component';
import { BoxComponent } from './box/box.component';
import { HeaderComponent } from './box/header/header.component';
import { InputComponent } from './box/input/input.component';
import { DisplayComponent } from './box/display/display.component';
import {  ProfileBoxComponent } from './profileBox/profilebox.component';

@NgModule({
  declarations: [
    AppComponent, NavigationComponent, ProfileComponent, NotificationsComponent, RoomsComponent, 
    DirectMessagesComponent, RoomComponent, ThreadComponent, DirectMessageComponent, BoxComponent,
    HeaderComponent, InputComponent, DisplayComponent, ProfileBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
